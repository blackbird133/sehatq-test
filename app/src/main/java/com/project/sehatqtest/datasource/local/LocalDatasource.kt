package com.project.sehatqtest.datasource.local

import com.project.sehatqtest.model.response.HomeResponse
import com.project.sehatqtest.model.stub.StubJson
import com.project.sehatqtest.util.PreferencesManager

class LocalDatasource {

    companion object {
        const val KEY_RESPONSE = ".responseBody"
        const val PRODUCT_LIST = ".productList"
    }

    var responseBody by PreferencesManager(KEY_RESPONSE, HomeResponse())
    var ids by PreferencesManager(PRODUCT_LIST, StubJson())

    fun clearResponseBody() {
        PreferencesManager.delete(KEY_RESPONSE)
    }
}