package com.project.sehatqtest.datasource.remote

import com.project.sehatqtest.model.response.HomeResponse
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET

interface DataSource {
    @GET("home")
    fun getHomeProductAsync() : Deferred<Response<List<HomeResponse>>>
}