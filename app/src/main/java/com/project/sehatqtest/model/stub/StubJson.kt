package com.project.sehatqtest.model.stub

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class StubJson(
    val ids: List<Int> = listOf()
) : Parcelable