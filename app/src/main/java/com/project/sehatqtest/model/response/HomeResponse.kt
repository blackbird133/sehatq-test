package com.project.sehatqtest.model.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class HomeResponse(
    @SerializedName("data") val data: DataResponse? = DataResponse()
) : Parcelable

@Parcelize
data class DataResponse(
    @SerializedName("category") val category: List<CategoryProduct>? = listOf(),
    @SerializedName("productPromo") val productPromo: List<ProductPromo>? = listOf()
) : Parcelable

@Parcelize
data class CategoryProduct(
    @SerializedName("imageUrl") val imageUrl: String = "",
    @SerializedName("id") val id: Int = 0,
    @SerializedName("name") val name: String = ""

) : Parcelable

@Parcelize
data class ProductPromo(
    @SerializedName("id") val id: Int = 0,
    @SerializedName("imageUrl") val imageUrl: String = "",
    @SerializedName("title") val title: String = "",
    @SerializedName("description") val description: String = "",
    @SerializedName("price") val price: String = "",
    @SerializedName("loved") val loved: Int = 0
) : Parcelable