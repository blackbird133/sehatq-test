package com.project.sehatqtest.application.module

import com.project.sehatqtest.repository.Repository
import org.koin.dsl.module

val repositoryModule = module {
    single { Repository(get()) }
}