package com.project.sehatqtest.application

import android.content.Context
import android.util.DisplayMetrics
import com.google.gson.Gson
import com.project.sehatqtest.model.ErrorResponse
import kotlinx.coroutines.Deferred
import retrofit2.Response
import kotlin.math.roundToInt

sealed class ResponseAPI<T> {
    data class Success<T>(val data: T) : ResponseAPI<T>()
    object NoContent : ResponseAPI<Nothing>()
    data class Error<T>(val responseCode: Int?, val errorResponse: ErrorResponse?) : ResponseAPI<T>()
    data class Exceptions<T>(val throwable: Throwable) : ResponseAPI<T>()
}

suspend fun <T> requestApi(request: () -> Deferred<Response<T>>): ResponseAPI<T?> {
    return try {
        val response = request.invoke().await()
        when {
            response.isSuccessful -> ResponseAPI.Success(response.body())
            else -> {
                val errorResponse: ErrorResponse = Gson().fromJson(
                    response.errorBody()?.string(),
                    ErrorResponse::class.java
                )

                ResponseAPI.Error(response.code(), errorResponse)
            }
        }
    } catch (exception: Exception) {
        ResponseAPI.Exceptions(exception)
    }
}

fun Context.convertDpToPx(dp: Int): Int {
    val displayMetrics: DisplayMetrics = resources.displayMetrics
    return dp.times(displayMetrics.xdpi.div(DisplayMetrics.DENSITY_DEFAULT)).roundToInt()
}