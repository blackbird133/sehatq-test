package com.project.sehatqtest.application

import android.app.Application
import com.orhanobut.hawk.Hawk
import com.project.sehatqtest.application.module.localDataSource
import com.project.sehatqtest.application.module.networkModule
import com.project.sehatqtest.application.module.repositoryModule
import com.project.sehatqtest.application.module.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class SehatQTest : Application() {

    private val modules = listOf(
        repositoryModule,
        viewModelModule,
        networkModule,
        localDataSource
    )

    override fun onCreate() {
        super.onCreate()
        initKoin()
    }

    private fun initKoin(){
        Hawk.init(this).build()
        startKoin {
            androidLogger(Level.ERROR)
            androidContext(this@SehatQTest)
            modules(modules)
        }
    }
}