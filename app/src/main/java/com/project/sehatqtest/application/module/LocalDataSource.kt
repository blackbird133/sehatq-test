package com.project.sehatqtest.application.module

import com.project.sehatqtest.datasource.local.LocalDatasource
import org.koin.dsl.module

val localDataSource = module {
    single { LocalDatasource() }
}