package com.project.sehatqtest.application.module

import com.project.sehatqtest.ui.home.viewmodel.HomeViewModel
import org.koin.dsl.module

val viewModelModule = module {
    single { HomeViewModel(get(), get()) }
}