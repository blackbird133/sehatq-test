package com.project.sehatqtest.ui.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.project.sehatqtest.R
import com.project.sehatqtest.databinding.FragmentDetailProductBinding
import com.project.sehatqtest.datasource.local.LocalDatasource

class ProductDetailFragment : Fragment() {

    private lateinit var binding: FragmentDetailProductBinding

    private val args: ProductDetailFragmentArgs by navArgs()

    private val localDatasource by lazy {
        LocalDatasource()
    }

    companion object {
        const val PURCHASED = "purchased"
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDetailProductBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        checkExtras()
    }

    private fun checkExtras() {
        val favStatus = args.favStatus
        val prodID = args.productID

        val prodTotal = localDatasource.responseBody.data?.productPromo?.size ?: 0

        for (i in 0 until prodTotal) {
            val product = localDatasource.responseBody.data?.productPromo?.get(i)
            if (product?.id == prodID) {
                binding.apply {
                    Glide.with(requireContext())
                        .load(product.imageUrl)
                        .placeholder(R.mipmap.ic_launcher)
                        .error(R.mipmap.ic_launcher)
                        .into(imgThumbnail)

                    txtProductName.text = product.title
                    txtDescription.text = product.description
                    txtPrice.text = product.price

                    if (favStatus) {
                        imgFav.setImageResource(R.drawable.ic_fav_clicked)
                    } else {
                        imgFav.setImageResource(R.drawable.ic_fav)
                    }

                    btnBuy.setOnClickListener {
                        findNavController().navigate(
                            ProductDetailFragmentDirections.detailFragmentToPurchasedFragment(
                                favStatus, prodID,
                                PURCHASED
                            )
                        )
//                        requireActivity().finish()
                    }
                }
            }
        }
    }
}