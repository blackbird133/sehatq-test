package com.project.sehatqtest.ui.home.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.project.sehatqtest.BaseViewModel
import com.project.sehatqtest.application.ResponseAPI
import com.project.sehatqtest.datasource.local.LocalDatasource
import com.project.sehatqtest.model.response.HomeResponse
import com.project.sehatqtest.repository.Repository
import kotlinx.coroutines.launch


class HomeViewModel(
    private val repository: Repository,
    private val localDatasource: LocalDatasource
) : BaseViewModel() {

    private val _homeProduct = MutableLiveData<List<HomeResponse>>()
    val homeProduct: LiveData<List<HomeResponse>> = _homeProduct

    fun getHomeProduct() =
        scope.launch {
            when (val result = repository.getHomeProduct()) {
                is ResponseAPI.Success -> {
                    _homeProduct.value = result.data
                    localDatasource.responseBody = result.data?.get(0) ?: HomeResponse()
                }
            }
        }


}