package com.project.sehatqtest.ui.home.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.project.sehatqtest.R
import com.project.sehatqtest.databinding.ItemCategoryBinding
import com.project.sehatqtest.model.response.CategoryProduct

class CategoryAdapter(private val context: Context?) :
    RecyclerView.Adapter<CategoryAdapter.ViewHolder>() {
    private lateinit var binding: ItemCategoryBinding

    var categories: MutableList<CategoryProduct> = mutableListOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryAdapter.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        binding = ItemCategoryBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: CategoryAdapter.ViewHolder, position: Int) {
        holder.bindData(categories[position], context)
    }

    override fun getItemCount(): Int = categories.size

    class ViewHolder(private val itemBinding: ItemCategoryBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        fun bindData(category: CategoryProduct, context: Context?) {
            itemBinding.apply {
                txtCat.text = category.name

                if (context != null) {
                    Glide.with(context)
                        .load(category.imageUrl)
                        .placeholder(R.mipmap.ic_launcher)
                        .error(R.mipmap.ic_launcher)
                        .into(imgCatThumbnail)
                }
            }
        }
    }
}