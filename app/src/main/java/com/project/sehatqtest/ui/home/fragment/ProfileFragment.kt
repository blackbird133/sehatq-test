package com.project.sehatqtest.ui.home.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.project.sehatqtest.databinding.FragmentProfileBinding
import com.project.sehatqtest.datasource.local.LocalDatasource
import com.project.sehatqtest.model.stub.StubJson
import com.project.sehatqtest.ui.home.adapter.ProductAdapter

class ProfileFragment : Fragment() {

    private lateinit var binding: FragmentProfileBinding
    private val args: ProfileFragmentArgs by navArgs()
    private val prodPurchasedIDs: MutableList<Int> = mutableListOf()

    private val localDatasource by lazy {
        LocalDatasource()
    }

    private val purchasedAdapter by lazy {
        ProductAdapter(requireContext(), {})
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentProfileBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initViews()
        setUpToolbar()
        checkExtras()
    }

    private fun setUpToolbar() {
        (activity as AppCompatActivity).apply {
            title = "Purchased History"
        }
    }

    override fun onResume() {
        super.onResume()

        checkExtras()
    }


    private fun initViews() {
        binding.apply {
            rvPurchased.apply {
                adapter = purchasedAdapter
                layoutManager = LinearLayoutManager(
                    context,
                    LinearLayoutManager.VERTICAL,
                    false
                )
            }
        }
    }

    private fun checkExtras() {
        val favStatus = args.favStatus
        val prodID = args.productID
        val prodStatus = args.statusProduct

        prodPurchasedIDs.add(prodID)
        val prodIDs = prodPurchasedIDs.distinct()
        localDatasource.ids = StubJson(ids = prodIDs)

        val prodTotal = localDatasource.responseBody.data?.productPromo

        for (i in localDatasource.ids.ids.indices) {
            purchasedAdapter.productList =
                prodTotal?.filter { it.id == localDatasource.ids.ids[i] }?.toMutableList()
                    ?: mutableListOf()
        }
    }
}