package com.project.sehatqtest.ui.home.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.project.sehatqtest.databinding.FragmentSearchBinding
import com.project.sehatqtest.datasource.local.LocalDatasource
import com.project.sehatqtest.ui.home.adapter.SearchAdapter

class SearchFragment : Fragment() {

    private lateinit var binding: FragmentSearchBinding
    private val searchAdapter by lazy {
        SearchAdapter(
            requireContext()
        ) {
            findNavController().navigate(
                SearchFragmentDirections.searchFragmentToDetailFragment(
                    false,
                    it.id
                )
            )
        }
    }

    private val localDatasource: LocalDatasource by lazy {
        LocalDatasource()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSearchBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initViews()
    }

    private fun initViews() {
        binding.apply {
            rvSearch.apply {
                adapter = searchAdapter
                layoutManager = LinearLayoutManager(
                    context,
                    LinearLayoutManager.VERTICAL,
                    false
                )
            }

            edtSearch.apply {
                setText("")
                doAfterTextChanged {
                    initData(edtSearch.text.toString())
                }
            }
        }
    }

    private fun initData(query: String = "") {
        if (query.isEmpty()) {
            searchAdapter.productList =
                localDatasource.responseBody.data?.productPromo?.toMutableList()
                    ?: mutableListOf()
        } else {
            searchAdapter.productList =
                localDatasource.responseBody.data?.productPromo?.filter { it.price == query }
                    ?.toMutableList()
                    ?: mutableListOf()
        }

    }
}