package com.project.sehatqtest.ui.home.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.project.sehatqtest.R
import com.project.sehatqtest.databinding.ItemProductBinding
import com.project.sehatqtest.model.response.ProductPromo

class SearchAdapter(
    private val context: Context?,
    private val itemClickListener: (ProductPromo) -> Unit
) : RecyclerView.Adapter<SearchAdapter.ViewHolder>() {

    private lateinit var binding: ItemProductBinding
    var productList: MutableList<ProductPromo> = mutableListOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        binding = ItemProductBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(productList[position], itemClickListener, context)
    }

    override fun getItemCount(): Int = productList.size

    class ViewHolder(private val itemBinding: ItemProductBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        fun bindData(
            productList: ProductPromo,
            itemClickListener: (ProductPromo) -> Unit,
            context: Context?
        ) {
            itemBinding.apply {
                txtProdName.text = productList.title
                txtProdPrice.text = productList.price

                if (context != null) {
                    Glide.with(context)
                        .load(productList.imageUrl)
                        .placeholder(R.mipmap.ic_launcher)
                        .error(R.mipmap.ic_launcher)
                        .into(imgCatThumbnail)
                }

                layoutRoot.setOnClickListener {
                    itemClickListener.invoke(productList)
                }
            }
        }
    }
}