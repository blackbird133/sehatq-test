package com.project.sehatqtest.ui.home.fragment

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.project.sehatqtest.application.convertDpToPx
import com.project.sehatqtest.databinding.FragmentHomeBinding
import com.project.sehatqtest.datasource.local.LocalDatasource
import com.project.sehatqtest.ui.home.adapter.CategoryAdapter
import com.project.sehatqtest.ui.home.adapter.ProductListAdapter
import com.project.sehatqtest.ui.home.viewmodel.HomeViewModel
import com.project.sehatqtest.util.SpaceDecorator
import org.koin.androidx.viewmodel.ext.android.viewModel

class HomeFragment : Fragment() {

    private lateinit var binding: FragmentHomeBinding
    private val viewModel: HomeViewModel by viewModel()
    private lateinit var mGoogleSignInClient: GoogleSignInClient

    private val localDatasource: LocalDatasource by lazy {
        LocalDatasource()
    }

    private val categoryAdapter by lazy {
        CategoryAdapter(requireContext())
    }

    private val productListAdapter by lazy {
        ProductListAdapter(
            { prod, fav ->
                findNavController().navigate(
                    HomeFragmentDirections.homeFragmentToDetailFragment(
                        fav,
                        prod.id
                    )
                )
            },
            requireContext()
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.getHomeProduct()

        initView()
        observeData()
    }

//    private fun initGoogleSignIn() {
//        val acct = GoogleSignIn.getLastSignedInAccount(requireActivity())
//        if (acct != null) {
//            val personName = acct.displayName
//            val personGivenName = acct.givenName
//            val personFamilyName = acct.familyName
//            val personEmail = acct.email
//            val personId = acct.id
//            val personPhoto: Uri = acct.photoUrl
//        }
//
//    }

    private fun signOutGoogle() {
        mGoogleSignInClient.signOut().addOnCompleteListener {
            Toast.makeText(requireContext(), "Successfully SignOut", Toast.LENGTH_SHORT).show()
            if (!findNavController().navigateUp())
                requireActivity().finish()
        }
    }

    private fun initView() {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build()

        mGoogleSignInClient = GoogleSignIn.getClient(requireActivity(), gso)

        binding.apply {
            rvCategory.apply {
                adapter = categoryAdapter
                layoutManager = LinearLayoutManager(
                    context,
                    LinearLayoutManager.HORIZONTAL,
                    false
                )
            }

            rvProduct.apply {
                adapter = productListAdapter
                layoutManager = LinearLayoutManager(
                    context,
                    LinearLayoutManager.VERTICAL,
                    false
                )
                addItemDecoration(SpaceDecorator(context.convertDpToPx(16)))
            }

            txtSearch.setOnClickListener {
                findNavController().navigate(HomeFragmentDirections.homeFragmentToSearchFragment())
            }

            imgFav.setOnClickListener {
                signOutGoogle()
            }
        }
    }

    private fun observeData() {
        viewModel.apply {
            homeProduct.observe(requireActivity(), {
                localDatasource.responseBody = it[0]
                categoryAdapter.categories =
                    localDatasource.responseBody.data?.category?.toMutableList() ?: mutableListOf()

                productListAdapter.productList =
                    localDatasource.responseBody.data?.productPromo?.toMutableList()
                        ?: mutableListOf()
            })
        }
    }
}