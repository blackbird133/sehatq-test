package com.project.sehatqtest.ui.home.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.project.sehatqtest.R
import com.project.sehatqtest.databinding.ItemProductListBinding
import com.project.sehatqtest.datasource.local.LocalDatasource
import com.project.sehatqtest.model.response.ProductPromo

class ProductListAdapter(
    private val itemClickListener: (ProductPromo, Boolean) -> Unit,
    private val context: Context?
) :
    RecyclerView.Adapter<ProductListAdapter.ViewHolder>() {

    private lateinit var binding: ItemProductListBinding

    var productList: MutableList<ProductPromo> = mutableListOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        binding = ItemProductListBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ProductListAdapter.ViewHolder, position: Int) {
        holder.bindData(
            productList[position],
            context,
            itemClickListener
        )
    }

    override fun getItemCount(): Int =
        productList.size

    class ViewHolder(private val itemBinding: ItemProductListBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        var favClicked = false

        fun bindData(
            productList: ProductPromo,
            context: Context?,
            itemClickListener: (ProductPromo, Boolean) -> Unit
        ) {
            itemBinding.apply {
                txtProdName.text = productList.title

                imgFav.apply {
                    if (productList.loved == 0) {
                        favClicked = false
                        setImageResource(R.drawable.ic_fav)
                        setOnClickListener {
                            if (!favClicked) {
                                favClicked = true
                                setImageResource(R.drawable.ic_fav_clicked)
                            } else {
                                favClicked = false
                                setImageResource(R.drawable.ic_fav)
                            }
                        }
                    } else {
                        favClicked = true
                        setImageResource(R.drawable.ic_fav_clicked)
                        setOnClickListener {
                            if (favClicked) {
                                favClicked = false
                                setImageResource(R.drawable.ic_fav)
                            } else {
                                favClicked = true
                                setImageResource(R.drawable.ic_fav_clicked)
                            }
                        }
                    }
                }

                if (context != null) {
                    Glide.with(context)
                        .load(productList.imageUrl)
                        .placeholder(R.mipmap.ic_launcher)
                        .error(R.mipmap.ic_launcher)
                        .into(imgProduct)
                }



                layoutRoot.setOnClickListener {
                    itemClickListener.invoke(productList, favClicked)
                }
            }
        }
    }
}