package com.project.sehatqtest.repository

import com.project.sehatqtest.application.requestApi
import com.project.sehatqtest.datasource.remote.DataSource

class Repository(private val dataSource: DataSource) {
    suspend fun getHomeProduct() = requestApi {
        dataSource.getHomeProductAsync()
    }
}