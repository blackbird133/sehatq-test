package com.project.sehatqtest.util

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class SpaceDecorator(
    private val spacing: Int,
    private val type: Int = TYPE_VERTICAL,
    private val columnCount: Int = 1,
    private val includeEdge: Boolean = true,
    private val includeTopEdge: Boolean = true
) : RecyclerView.ItemDecoration() {

    companion object {
        const val TYPE_HORIZONTAL = 0
        const val TYPE_VERTICAL = 1
        const val TYPE_GRID = 2
    }

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        val position = parent.getChildAdapterPosition(view)
        val column = if (type == TYPE_GRID) {
            position % columnCount
        } else {
            columnCount
        }

        when (type) {
            TYPE_HORIZONTAL -> {
                val isLastItem = position == parent.adapter?.itemCount?.minus(1)
                when {
                    position == 0 -> {
                        outRect.left = spacing
                        outRect.right = spacing.div(2)
                    }

                    isLastItem -> {
                        outRect.left = spacing.div(2)
                        outRect.right = spacing
                    }

                    else -> {
                        outRect.left = spacing.div(2)
                        outRect.right = spacing.div(2)
                    }
                }

                outRect.top = spacing
                outRect.bottom = spacing
            }

            TYPE_VERTICAL -> {
                val isLastItem = position == parent.adapter?.itemCount?.minus(1)
                when {
                    position == 0 -> {
                        outRect.top = spacing
                        outRect.bottom = spacing.div(2)
                    }

                    isLastItem -> {
                        outRect.top = spacing.div(2)
                        outRect.bottom = spacing
                    }

                    else -> {
                        outRect.top = spacing.div(2)
                        outRect.bottom = spacing.div(2)
                    }
                }

                outRect.left = spacing
                outRect.right = spacing
            }

            TYPE_GRID -> {
                if (includeEdge) {
                    outRect.left = spacing - column * spacing / columnCount
                    outRect.right = (column + 1) * spacing / columnCount
                    if (position < columnCount && includeTopEdge) {
                        outRect.top = spacing
                    }

                    outRect.bottom = spacing
                } else {
                    outRect.left = column * spacing / columnCount
                    outRect.right = spacing - (column + 1) * spacing / columnCount

                    if (position >= columnCount) {
                        outRect.top = spacing
                    }
                }
            }
        }
    }
}