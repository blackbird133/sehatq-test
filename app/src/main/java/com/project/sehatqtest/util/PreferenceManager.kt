package com.project.sehatqtest.util

import com.orhanobut.hawk.Hawk
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty


class PreferencesManager<T>(
    private val key: String,
    private val defaultValue: T
) : ReadWriteProperty<Any, T> {

    override fun setValue(thisRef: Any, property: KProperty<*>, value: T) {
        Hawk.put(key, value)
    }

    override fun getValue(thisRef: Any, property: KProperty<*>): T {
        return Hawk.get(key, defaultValue)
    }

    companion object {
        fun delete(key: String) {
            Hawk.delete(key)
        }
    }
}